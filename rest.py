from flask import Flask
import RPi.GPIO as GPIO
from time import sleep

app = Flask(__name__)


GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(12, GPIO.OUT)


@app.route('/')
def bell():
	GPIO.output(12, 0)
	sleep(0.2)
	GPIO.output(12, 1)
	GPIO.cleanup()
	return 'bell rang'

if __name__ == '__main__':
	app.run(debug=True)
