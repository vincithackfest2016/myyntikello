To use this app, make changes to following lines:


```
#!python

GPIO.setup("GPIO pin of your choise", GPIO.OUT)

@app.route('/')
def bell():
        GPIO.output("GPIO pin of your choise", 0)
        sleep("Time required for the marble rollercoaster")
        GPIO.output("GPIO pin of your choise", 1)
        GPIO.cleanup()
        return 'bell rang'
```